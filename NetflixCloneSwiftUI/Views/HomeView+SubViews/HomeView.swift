import SwiftUI

// MARK: - Preview
struct HomeView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        HomeView()//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        //.previewLayout(.fixed(width: 440, height: 270))
    }
}

struct HomeView: View {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    var vm = HomeVM()
    
    /*=============================*/
    
    // MARK: -∂ Helper methods
    
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>ZSTACK||
        //*****************************/
        ZStack {
            
            Color.black
                .edgesIgnoringSafeArea(.all)
            
            ScrollView(showsIndicators: false) {
                //__________
                // MARK: - LazyVStack only loads what is being used
                /**---------------------------------*/
                LazyVStack {
                    
                    // MARK: -∂ Logo
                    TopRowButtonsV()

                    // MARK: -∂ Top View
                    TopMoviePreviewV(movie: exampleMovie3)
                        .frame(width: screen.width)
                        // Will push the preview to the top of the screen
                        .padding(.top, -110)
                        // Moves the logo on top of the top view
                        .zIndex(-1)
                    
                    //__________
                    // MARK: - Child-ForEach looping through our categories
                    /**---------------------------------*/
                    /// - ©id: \.self: Hashes the category values with its own id
                    ForEach(vm.allCategories, id: \.self) { category in
                        //__________
                        VStack {
                            ///_CHILD__=>HStack<--VStack
                            HStack {
                                //__________
                                // MARK: -∂ Category
                                Text(category)
                                    .font(.title3)
                                    .bold()
                                    .padding(.leading, 5)
                                
                                Spacer()// Spaced horizontally
                                
                                
                            }//|>END OF >> HStack||
                            /**---------------------------------*/
                            
                            ///_CHILD__=>ScrollView<--VStack
                            ScrollView(.horizontal, showsIndicators: false) {
                                //__________
                                LazyHStack {
                                    //__________
                                    ForEach(vm.getMovies(forCategory: category)) { movie in
                                        //__________
                                        StandardHomeMovieV(movie: movie)
                                            .frame(width: 100, height: 200)
                                            .padding(.horizontal, 20)
                                        
                                    }//|>END OF >> ForEach||
                                    
                                    
                                }///|>END OF >> LazyHStack||
                                
                                /*©---------------------------------©*/
                                
                            }//|>END OF >> ScrollView||
                            
                            /**---------------------------------*/
                           
                        }//|>END OF >> VStack||
                        
                        /*©---------------------------------©*/
                        
                    }//|>END OF >> ForEach||
                    
                    /*©---------------------------------©*/
                    
                }///|>END OF >> LazyVStack||
                
                /*©---------------------------------©*/
                
            }//|>END OF >> ScrollView||
            
            /**---------------------------------*/
            
        }//||END__PARENT-ZSTACK||
        .foregroundColor(.white)
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

