import SwiftUI

struct TopRowButtonsV: View {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    
    /*=============================*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //*****************************/
        HStack {
            //__________
            // MARK: -∂ Netflix Logo
            Button(action: {
                //__________
                
            }, label: {
                //__________
                Image("netflix_logo")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 50)
                
            })
            Spacer()// Spaced horizontally
            
            // MARK: -∂ TV Shows
            Button(action: {
                //__________
                
            }, label: {
                //__________
                Text("TV Shows")
                    .bold()

            })
            Spacer()// Spaced horizontally
            
            // MARK: -∂ Movies
            Button(action: {
                //__________
                
            }, label: {
                //__________
                Text("Movies")
                    .bold()

            })
            Spacer()// Spaced horizontally
            
            // MARK: -∂ My List
            Button(action: {
                //__________
                
            }, label: {
                //__________
                Text("My List")
                    .bold()
            })
            
        }//||END__PARENT-VSTACK||
        .padding(.leading, 30)
        .padding(.trailing, 30)
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

