import SwiftUI

// MARK: - Preview
struct SmallVerticalButtonV_Previews: PreviewProvider {
    
    static var previews: some View {
        
        SmallVerticalButtonV(text: "My List",
                             sfIsOnImage: "checkmark",
                             sfIsOffImage: "plus",
                             isOn: false) {
            //__________
            printf("Tapped")
        }
        //.padding(.all, 100)
        .preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        .previewLayout(.fixed(width: 440, height: 270))
    }
}

struct SmallVerticalButtonV: View {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    var text: String
    var sfIsOnImage, sfIsOffImage: String
    var isOn: Bool
    
    /// - ©Computed property
    var imageName: String {
        //__________
        if isOn {
            //__________
            return sfIsOnImage
            
        } else { return sfIsOffImage }
    }
    
    /// - ©Closure property
    var action: () -> Void
    /*=============================*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //*****************************/
        VStack(spacing: 8.0) {
            
            Button(action: action,
                   //__________
                   label: {
                    //__________
                    VStack {
                        /// - ©SFSymbol
                        Image(systemName: imageName)
                            .foregroundColor(.white)
                            .padding(.bottom, 4)
                        
                        /// - ©My List
                        Text(text)
                            .foregroundColor(.white)
                            .font(.system(size: 14))
                            .bold()
                    }
                   })
            
        }//||END__PARENT-VSTACK||
        
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

