import SwiftUI

// MARK: - Preview
struct WhitePlayButtonV_Previews: PreviewProvider {
    
    static var previews: some View {
        
        WhitePlayButtonV(text: "Play", sfImageName: "play.fill") {
            //__________
            printf("Tapped")
        }
        //.padding(.all, 100)
        .preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        .previewLayout(.fixed(width: 440, height: 270))
    }
}

struct WhitePlayButtonV: View {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    var text, sfImageName: String
    
    /// - ©Closure property
    var action: () -> Void
    /*=============================*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //*****************************/
        VStack(spacing: 8.0) {
            
            Button(action: action,
                   //__________
                   label: {
                    //__________
                    HStack {
                        //__________
                        Spacer()// Spaced horizontally
                        
                        /// - ©SFSymbol
                        Image(systemName: sfImageName)
                            .font(.headline)
                        
                        /// - ©Play text
                        Text(text)
                            .bold()
                            .font(.system(size: 16))
                        
                        Spacer()// Spaced horizontally
                        
                    }
                    .padding(.vertical, 6)
                    .foregroundColor(.black)
                    .background(Color.white)
                    .cornerRadius(3.0)
                    /*©---------------------------------©*/
                    
                   }
            )///|>END OF >> Button||
            
        }//||END__PARENT-VSTACK||
        
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

