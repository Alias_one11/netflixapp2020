import SwiftUI
import KingfisherSwiftUI

// MARK: - Preview
struct StandardHomeMovieV_Previews: PreviewProvider {
    
    static var previews: some View {
        
        StandardHomeMovieV(movie: exampleMovie1)//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        //.previewLayout(.fixed(width: 440, height: 270))
    }
}

struct StandardHomeMovieV: View {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    var movie: Movie
    /*=============================*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //*****************************/
        VStack(spacing: 8.0) {
            
            KFImage(movie.thumbnailURL)
                .resizable()
                .scaledToFill()
            
        }//||END__PARENT-VSTACK||
        
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

