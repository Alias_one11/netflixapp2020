import SwiftUI

struct MovieInfoSubHeadline: View {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    var movie: Movie
    /*=============================*/
    
    var body: some View {
        
        HStack(spacing: 20) {
            //__________
            // MARK: -∂ SFSymbol thumbs up
            Image(systemName: "hand.thumbsup.fill")
                .foregroundColor(.white)
            
            // MARK: -∂ Movie year
            Text(String(movie.year))
            
            // MARK: -∂ Rating
            RatingsView(rating: movie.rating)
            
            // MARK: -∂ Seasons
            Text(movie.numberOfSeasonsDisplayed)
            
        }
        .foregroundColor(.gray)
        .padding(.vertical, 8)
    }
}
