import SwiftUI

// MARK: - Preview
struct MovieDetailView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        MovieDetailView(movie: exampleMovie1)//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        //.previewLayout(.fixed(width: 440, height: 270))
    }
}

struct MovieDetailView: View {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    var movie: Movie
    
    /*=============================*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>ZStack||
        //*****************************/
        ZStack {
            
            Color.black
                .edgesIgnoringSafeArea(.all)
            
            // MARK: - Child-VStack
            /**---------------------------------*/
            VStack {
                //__________
                HStack {
                    //__________
                    Spacer()// Spaced horizontally
                    
                    Button(action: {
                        //__________
                        
                    }, label: {
                        //__________
                        // MARK: -∂ X-Mark
                        Image(systemName: "xmark.circle")
                            .font(.system(size: 28))
                    })
                    
                }///|>END OF >> HStack||
                .padding(.horizontal, 20)
                /*©---------------------------------©*/
                
                ///_CHILD__=>ScrollView<--VStack
                /**---------------------------------*/
                ScrollView(.vertical, showsIndicators: false) {
                    //__________
                    VStack {
                        //__________
                        // MARK: -∂ Standard movie
                        StandardHomeMovieV(movie: movie)
                            .frame(width: screen.width / 2.5)
                        
                        // MARK: - MovieInfoSubHeadline
                        MovieInfoSubHeadline(movie: movie)
                        
                        // MARK: -∂ Bottom headline
                        if movie.promotionHeadline != nil {
                            //__________
                            Text(movie.promotionHeadline!)
                                .bold()
                                .font(.headline)
                        }
                        
                    }//|>END OF >> VStack||
                    
                    /*©---------------------------------©*/
                    
                }///|>END OF >> ScrollView||
                /**---------------------------------*/
                
                Spacer()// Spaced vertically
                
            }//|>END OF >> VStack||
            .foregroundColor(.white)
            /**---------------------------------*/
            
        }//||END__PARENT-ZStack||
        
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]


///*©-----------------------------------------------------------©*/

