import SwiftUI

struct RatingsView: View {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    var rating: String
    /*=============================*/
    
    var body: some View {
        
        ZStack {
            //__________
            // Is going to lay everything on tope of each other
            Rectangle()
                .foregroundColor(.gray)
            
            // MARK: -∂ TV Rating in ZStack
            Text(rating)
                .foregroundColor(.white)
                .font(.system(size: 12))
                .bold()
            
        }
        .frame(width: 50, height: 20)
        /*©---------------------------------©*/
    }
}
