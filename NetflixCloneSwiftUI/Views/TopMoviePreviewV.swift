import SwiftUI
import KingfisherSwiftUI

// MARK: - Preview
struct TopMoviePreviewV_Previews: PreviewProvider {
    
    static var previews: some View {
        
        TopMoviePreviewV(movie: exampleMovie6)//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        .previewLayout(.fixed(width: 380, height: 640))
        // The preview below is for like a card
        //.previewLayout(.fixed(width: 440, height: 270))
    }
}

struct TopMoviePreviewV: View {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    var movie: Movie
    /*=============================*/
    
    // MARK: -∂ Class methods
    func isCategoryLast(_ category: String) -> Bool {
        //__________
        let categoryCount = movie.categories.count // 4
        
        /// if index-->0 + 1 != 4 return false
        if let index = movie.categories.firstIndex(of: category) {
            //__________
            if index + 1 != categoryCount {
                return false
            }
        }
        
        return true
    }
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>ZStack||
        //*****************************/
        ZStack {
            
            KFImage(movie.thumbnailURL)
                .resizable()
                .scaledToFill()
                .clipped()
            
            // MARK: - VStack
            /**---------------------------------*/
            VStack {
                //__________
                Spacer()// Spaced vertically
                
                HStack {
                    //__________
                    ForEach(movie.categories, id: \.self) { category in
                        //__________
                        HStack {
                            Text(category)
                                .font(.footnote)
                            
                            // Removes the last blue dot on the category
                            if !isCategoryLast(category) {
                                //__________
                                Image(systemName: "circle.fill")
                                    .foregroundColor(.blue)
                                    .font(.system(size: 3))
                            }
                            
                            
                        }///|>END OF >> HStack||
                        .padding(.bottom, 3)
                        /*©---------------------------------©*/
                        
                    }//|>END OF >> ForEach||
                    
                }///|>END OF >> HStack||
                
                /*©---------------------------------©*/
                
                ///_CHILD__=>HStack<--VStack
                /**---------------------------------*/
                HStack {
                    //__________
                    Spacer()// Spaced horizontally
                    
                    // MARK: -∂ MyList Button
                    SmallVerticalButtonV(text: "My List",
                                         sfIsOnImage: "checkmark",
                                         sfIsOffImage: "plus",
                                         isOn: true) {
                        //__________
                        
                    }
                    
                    Spacer()// Spaced horizontally
                    // MARK: -∂ Play Button
                    WhitePlayButtonV(text: "Play", sfImageName: "play.fill") {
                        //__________
                        printf("Tapped")
                    }
                    .frame(width: 120)
                    Spacer()// Spaced horizontally
                    
                    // MARK: -∂ Info Button
                    SmallVerticalButtonV(text: "Info", sfIsOnImage: "info.circle", sfIsOffImage: "info.circle", isOn: true) {
                        //__________
                        
                    }
                    
                    Spacer()// Spaced horizontally
                }
                
                /**---------------------------------*/
            }//|>END OF >> VStack||
            .background(
                //__________
                LinearGradient.blackLinearGradient
                    // Adjust the starting point of the gradient colors
                    .padding(.top, 250)
            )
            /**---------------------------------*/
            
        }//||END__PARENT-ZStack||
        .foregroundColor(.white)
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

