//
//  NetflixCloneSwiftUIApp.swift
//  NetflixCloneSwiftUI
//
//  Created by Jose Martinez on 8/22/20.
//

import SwiftUI

@main
struct NetflixCloneSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}
