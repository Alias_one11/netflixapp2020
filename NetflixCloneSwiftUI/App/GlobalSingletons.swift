//  GlobalSingletons.swift
//  NetflixCloneSwiftUI


import SwiftUI
import Foundation

// MARK: -∂ Static data
/*©---------------------------------©*/

let exampleMovie1 = Movie(
    name: "DARK",
    thumbnailURL: URL(string: "https://picsum.photos/200/300")!,
    categories: ["Dystopian", "Exciting", "Suspenseful", "Sci-Fi TV"],
    year: 2020, rating: "TV-MA", numberOfSeasons: 1,
    promotionHeadline: "Watch The Finally Of Season 1"
)
let exampleMovie2 = Movie(
    name: "Travelers",
    thumbnailURL: URL(string: "https://picsum.photos/200/300/")!,
    categories: ["Dystopian", "Exciting", "Suspenseful", "Sci-Fi TV"],
    year: 2020, rating: "TV-MA", numberOfSeasons: 2,
    promotionHeadline: "Watch Season 2 Now"
)
let exampleMovie3 = Movie(
    name: "Community",
    thumbnailURL: URL(string: "https://picsum.photos/200/301")!,
    categories: ["Dystopian", "Exciting", "Suspenseful", "Sci-Fi TV"],
    year: 2020, rating: "TV-MA", numberOfSeasons: 3,
    promotionHeadline: "Long Awaited Season Premiere Soon!"
)
let exampleMovie4 = Movie(
    name: "Alone",
    thumbnailURL: URL(string: "https://picsum.photos/200/302")!,
    categories: ["Dystopian", "Exciting", "Suspenseful", "Sci-Fi TV"],
    year: 2020, rating: "TV-MA", numberOfSeasons: 4,
    promotionHeadline: "New Episodes Coming Soon!"
)
let exampleMovie5 = Movie(
    name: "Hannibal",
    thumbnailURL: URL(string: "https://picsum.photos/200/303")!,
    categories: ["Dystopian", "Exciting", "Suspenseful", "Sci-Fi TV"],
    year: 2020, rating: "TV-MA", numberOfSeasons: 5,
    promotionHeadline: "Watch Season 5 Now"
)
let exampleMovie6 = Movie(
    name: "After Life",
    thumbnailURL: URL(string: "https://picsum.photos/200/304")!,
    categories: ["Dystopian", "Exciting", "Suspenseful", "Sci-Fi TV"],
    year: 2020, rating: "TV-MA", numberOfSeasons: 6,
    promotionHeadline: "Watch Season 6 Now"
)

let exampleMovieList: [Movie] = [
    exampleMovie1, exampleMovie2, exampleMovie3,
    exampleMovie4, exampleMovie5, exampleMovie6
]

/// - ©Helps with dealing with the width of the screen or height
let screen: CGRect = UIScreen.main.bounds
///*©-----------------------------------------------------------©*/

extension LinearGradient {
    //__________
    static let blackLinearGradient =
        LinearGradient(
            gradient:
                Gradient(
                    colors: [
                        Color.black.opacity(0.0),
                        Color.black.opacity(0.95)
                    ]
                ),
            startPoint: .top, endPoint: .bottom)
}
