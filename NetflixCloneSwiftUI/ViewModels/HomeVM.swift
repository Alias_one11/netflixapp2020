import SwiftUI

class HomeVM: ObservableObject {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    /// - ©Dictionary of movies [ Categories : [MovieList] ]
    @Published var moviesDict: [String : [Movie]] = [ : ]
    
    /// - ©Computed property array
    var allCategories: [String] {
        //__________
        /// - ©Mapping our dictionary string categories
        moviesDict.keys.map { String($0) }
    }
    /*=============================*/
    
    /// - ©Initializer
    init() {
        //__________
        setupMovies()
    }
    
    /// - ©Class methods
    /*©---------------------------------©*/
    
    func getMovies(forCategory category: String) -> [Movie] {
        //__________
        moviesDict[category] ?? []
    }
    
    func setupMovies() {
        //__________
        moviesDict["Trending Now"] = exampleMovieList
        moviesDict["Standup Comedy"] = exampleMovieList.shuffled()
        moviesDict["New Releases"] = exampleMovieList.shuffled()
        moviesDict["Watch It Again"] = exampleMovieList.shuffled()
        moviesDict["SciFi Movies"] = exampleMovieList.shuffled()
    }
    
    
    /*©---------------------------------©*/
    
}///|>END OF >> Class||
