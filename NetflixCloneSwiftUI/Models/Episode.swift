import SwiftUI

struct Episode: Identifiable {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    var id: String = UUID().uuidString
    
    var name: String
    var season: Int
    var thumbnailImageURLStr: String
    var description: String
    var length: Int
    
    /// - ©Computed Property to handle our thumbnailImageURLStr
    var thumbnailURL: URL {
        //__________
        URL(string: thumbnailImageURLStr)!
    }
    /*=============================*/
}
