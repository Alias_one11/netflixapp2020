import SwiftUI

struct Movie: Identifiable {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    var id: String = UUID().uuidString
    //__________
    var name: String
    var thumbnailURL: URL
    var categories: [String]
    
    /// - ©MovieDetailView properties
    var year: Int
    var rating: String
    var numberOfSeasons: Int?
    var episodes: [Episode]?
    var promotionHeadline: String?
    
    /// - ©Computed property to handle numberOfSeasons?
    var numberOfSeasonsDisplayed: String {
        //__________
        let oneSeason = "1 season"
        
        if let seasonCountStr = numberOfSeasons {
            //__________
            if seasonCountStr == 1 {
                //__________
                return oneSeason
            } else {
                return "\(seasonCountStr) seasons"
            }
        }
        
        return ""
    }
    /*=============================*/
}
